import 'package:chris_portfolio/info.dart';
import 'package:chris_portfolio/pages/home/page_home_desktop.dart';
import 'package:chris_portfolio/pages/home/page_home_mobile.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class PageHome extends StatelessWidget {
  static const name = Info.pageNameHome;

  const PageHome({super.key});

  @override
  Widget build(BuildContext context) {

    return ScreenTypeLayout.builder(
      // breakpoints: const ScreenBreakpoints(
      //   desktop: 1000,
      //   tablet: 900,
      //   watch: 300
      // ),
      mobile: (context) => OrientationLayoutBuilder(
        portrait:  (context) => const PageHomeMobilePort(),
        landscape: (context) => const PageHomeMobileLand(),
      ),
      desktop: (context) => const PageHomeDesktop(),
      //watch: (BuildContext context) => _getMobileLayout(theme),
      tablet: (context) => const PageHomeDesktop(),
    );
  }
}
