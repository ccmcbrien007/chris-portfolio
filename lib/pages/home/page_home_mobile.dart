import 'package:chris_portfolio/info.dart';
import 'package:chris_portfolio/pages/home/page_home.dart';
import 'package:chris_portfolio/utils/portfolio_navigator.dart';
import 'package:flutter/material.dart';

class PageHomeMobile extends StatelessWidget {

  const PageHomeMobile({super.key});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(flex: 1, child: Container()),
        Center(child: Image.network(width: size.shortestSide / 2, 'assets/images/chrismcbrien.png')),
        Flexible(flex: 1, child: Container()),
        ..._buildQuote(context),
        Flexible(flex: 1, child: Container()),
        ..._buildFooter(context, theme),
      ],
    );
  }

  List<Widget> _buildQuote(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return [
      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Text(
                softWrap: true,
                Info.quote,
                textAlign: TextAlign.right,
                style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
          ),
        )
      ]),
      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Text(
                softWrap: true,
                Info.quoteAttribution,
                textAlign: TextAlign.right,
                style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
          ),
        )
      ]),
    ];
  }

  List<Widget> _buildFooter(BuildContext context, ThemeData theme) {
    return [
      Row(children: [
        Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Text(
                softWrap: true,
                Info.appTitle,
                textAlign: TextAlign.left,
                style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
          ),
        )
      ]),
      Row(children: [
        Flexible(
            child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
          child: Text(
              softWrap: true,
              Info.appSubTitle,
              textAlign: TextAlign.left,
              style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
        ))
      ]),
      Row(children: [
        Flexible(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                child: Text(Info.designedIn,
                    style: theme.textTheme.labelSmall?.copyWith(
                      color: Colors.white,
                    ))))
      ])
    ];
  }
}

class PageHomeMobilePort extends StatelessWidget {
  const PageHomeMobilePort({super.key});

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text(Info.appTitle),
      ),
      backgroundColor: Colors.black,
      drawer: PortfolioNavigator.getMobileDrawer(context, PageHome.name),
      body: const PageHomeMobile(),
    );
  }
}

class PageHomeMobileLand extends StatelessWidget {
  const PageHomeMobileLand({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var theme = Theme.of(context);

    return Scaffold(
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          title: const Text('Chris McBrien - Professional Portfolio'),
        ),
        drawer: PortfolioNavigator.getMobileDrawer(context, PageHome.name),
        body: Row(
          children: [
            Flexible(flex: 1, child: Container()),
            Row(
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(8, 0, 16, 0),
                  width: size.longestSide / 3,
                  child: Text(
                      softWrap: true,
                      '${Info.quote}\n${Info.quoteAttribution}',
                      textAlign: TextAlign.right,
                      style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
                ),
                Center(child: Image.network(width: size.shortestSide / 2, 'assets/images/chrismcbrien.png')),
                Container(
                  padding: const EdgeInsets.fromLTRB(8, 0, 16, 0),
                  width: size.longestSide / 3,
                  child: Text(
                      softWrap: true,
                      '${Info.appTitle}\n${Info.appSubTitle}\n\n${Info.designedIn}',
                      textAlign: TextAlign.left,
                      style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
                ),
              ],
            ),
            Flexible(flex: 1, child: Container()),
          ],
        ));
  }
}
