import 'package:chris_portfolio/info.dart';
import 'package:chris_portfolio/pages/home/page_home.dart';
import 'package:chris_portfolio/utils/portfolio_navigator.dart';
import 'package:flutter/material.dart';

class PageHomeDesktop extends StatefulWidget {
  const PageHomeDesktop({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageHomeState();
  }
}

class _PageHomeState extends State<PageHomeDesktop> {
  String selectedItem = PageHome.name;

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PortfolioNavigator.getAppBar(context, PageHome.name),
      body: Column(
        children: [
          Flexible(flex: 1, child: Container()),
          Image.network(width: screenSize.width / 2, height: screenSize.height / 2,'assets/images/chrismcbrien.png'),
          Flexible(flex: 1, child: Container()),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildFooter(context, theme),
              Flexible(flex: 1, child: Container()),
              buildQuote(context),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildFooter(BuildContext context, ThemeData theme) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(Info.appTitle,
              textAlign: TextAlign.left, style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
          Text(Info.appSubTitle,
              textAlign: TextAlign.left, style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
          const SizedBox(height: 16),
          Text(
            Info.designedIn,
            style: theme.textTheme.labelSmall?.copyWith(
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildQuote(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
      child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.end, children: [
        Text(Info.quote, textAlign: TextAlign.left, style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
        Text(Info.quoteAttribution,
            textAlign: TextAlign.left, style: theme.textTheme.labelLarge?.copyWith(color: Colors.white)),
      ]),
    );
  }
}
