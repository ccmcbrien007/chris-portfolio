import 'package:chris_portfolio/pages/experience/page_experience_desktop.dart';
import 'package:chris_portfolio/pages/experience/page_experience_mobile.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../info.dart';

class PageExperience extends StatelessWidget {

  static const name = Info.pageNameExperience;

  const PageExperience({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      breakpoints: const ScreenBreakpoints(desktop: 1000, tablet: 900, watch: 300),
      mobile: (context) =>
          OrientationLayoutBuilder(
              portrait: (context) => const PageExperienceMobilePort(), landscape: (context) => const PageExperienceMobileLand()),
      desktop: (context) => const PageExperienceDesktop(),
      //watch: (BuildContext context) => _getMobileLayout(theme),
      tablet: (context) => const PageExperienceDesktop(),
    );
  }
}
