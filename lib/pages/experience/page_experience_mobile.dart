import 'package:chris_portfolio/pages/experience/page_experience.dart';
import 'package:flutter/material.dart';
import 'package:timelines/timelines.dart';

import '../../info.dart';
import '../../utils/portfolio_navigator.dart';

abstract class PageExperienceMobile extends StatefulWidget {
  const PageExperienceMobile({super.key});
}

abstract class _PageExperienceMobileState extends State<PageExperienceMobile> {
  bool animationDone = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text(Info.appTitle),
      ),
      drawer: PortfolioNavigator.getMobileDrawer(context, PageExperience.name),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TweenAnimationBuilder<int>(
              tween: IntTween(begin: 100, end: 0),
              curve: Curves.easeOut,
              duration: const Duration(seconds: 1),
              builder: (BuildContext context, int value, Widget? child) {
                return Expanded(
                  flex: value,
                  child: Container(color: Colors.black),
                );
              },
              onEnd: () => setState(() {
                animationDone = true;
              }),
            ),
            Expanded(
              flex: 100,
              child: Container(
                padding: const EdgeInsets.fromLTRB(5, 16, 5, 5),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: !animationDone ? Container() : _getPageContent(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getPageContent(BuildContext context) {
    return Timeline.tileBuilder(
      builder: TimelineTileBuilder.connectedFromStyle(
        contentsAlign: ContentsAlign.reverse,
        oppositeContentsBuilder: (context, index) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                        Text(Info.experiences[index].company, style: Theme.of(context).textTheme.titleLarge),
                        Text(Info.experiences[index].role, style: Theme.of(context).textTheme.titleSmall),
                      ])),
                  ...Info.experiences[index].responsibilities.map((e) => Row(
                        children: [
                          const Icon(Icons.arrow_right),
                          Expanded(child: Text(e)),
                        ],
                      ))
                ],
              ),
            ],
          ),
        ),
        contentsBuilder: (context, index) =>
            Container(padding: const EdgeInsets.fromLTRB(0, 0, 15, 0), child: Text(Info.experiences[index].dateRange)),
        itemCount: Info.experiences.length,
        connectorStyleBuilder: (context, index) => ConnectorStyle.solidLine,
        indicatorStyleBuilder: (context, index) => IndicatorStyle.dot,
      ),
    );
  }
}

class PageExperienceMobilePort extends PageExperienceMobile {
  const PageExperienceMobilePort({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageExperienceMobilePortState();
  }
}

class _PageExperienceMobilePortState extends _PageExperienceMobileState {}

class PageExperienceMobileLand extends PageExperienceMobile {
  const PageExperienceMobileLand({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageExperienceMobileLandState();
  }
}

class _PageExperienceMobileLandState extends _PageExperienceMobileState {}
