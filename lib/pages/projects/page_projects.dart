import 'package:chris_portfolio/pages/projects/page_projects_desktop.dart';
import 'package:chris_portfolio/pages/projects/page_projects_mobile.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../info.dart';

class PageProjects extends StatelessWidget {

  static const name = Info.pageNamePortfolio;

  const PageProjects({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      breakpoints: const ScreenBreakpoints(desktop: 1000, tablet: 900, watch: 300),
      mobile: (context) => OrientationLayoutBuilder(
          portrait: (context) => const PageProjectsMobilePort(),
          landscape: (context) => const PageProjectsMobileLand()),
      desktop: (context) => const PageProjectsDesktop(),
      //watch: (BuildContext context) => _getMobileLayout(theme),
      tablet: (context) => const PageProjectsDesktop(),
    );
  }
}
