import 'package:chris_portfolio/model/portfolio_info.dart';
import 'package:chris_portfolio/pages/projects/page_project_details.dart';
import 'package:chris_portfolio/pages/projects/page_projects.dart';
import 'package:flutter/material.dart';

import '../../info.dart';
import '../../utils/fade_in_card.dart';
import '../../utils/portfolio_navigator.dart';

class PageProjectsDesktop extends StatefulWidget {
  const PageProjectsDesktop({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageProjectsState();
  }
}

class _PageProjectsState extends State<PageProjectsDesktop> {
  bool animationDone = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: PortfolioNavigator.getAppBar(context, PageProjects.name),
      body: Center(
        child: Row(
          children: <Widget>[
            TweenAnimationBuilder<int>(
              tween: IntTween(begin: 100, end: 10),
              curve: Curves.easeOut,
              duration: const Duration(seconds: 1),
              builder: (BuildContext context, int value, Widget? child) {
                return Expanded(
                  flex: value,
                  child: Container(color: Colors.black),
                );
              },
              onEnd: () => setState(() {
                animationDone = true;
              }),
            ),
            Expanded(
              flex: 100,
              child: Container(
                padding: const EdgeInsets.fromLTRB(15, 16, 5, 5),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: !animationDone ? Container() : _getPageContent(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getPageContent(BuildContext context) {
    List<Widget> cards = _buildPortfolioCards();

    return GridView.count(primary: true, crossAxisCount: 2, children: [
      ...cards,
    ]);
  }

  List<Widget> _buildPortfolioCards() {
    Size screenSize = MediaQuery.of(context).size;
    double widgetWidth = screenSize.width * 0.9;
    double widgetHeight = screenSize.height * 0.4;

    return Info.portfolioItems
        .map((info) => InkWell(
              onTap: () {
                _openRadicalCardDetail(info);
              },
              child: Stack(
                children: [
                  Card(
                    child: Container(
                      width: widgetWidth,
                      height: widgetHeight,
                      padding: const EdgeInsets.all(5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.network(
                            width: widgetWidth,
                            height: widgetHeight * 0.5,
                            "assets/images/${info.imagePath}",
                            fit: BoxFit.contain,
                          ),
                          Text(info.name, style: Theme.of(context).textTheme.labelLarge),
                          Text("[${info.tech}]", style: Theme.of(context).textTheme.labelMedium),
                          Text(info.description,
                              textAlign: TextAlign.center, style: Theme.of(context).textTheme.labelSmall),
                        ],
                      ),
                    ),
                  ),
                  FadeInCard(
                    width: widgetWidth,
                    height: widgetHeight,
                    duration: 500,
                    child: Container(
                      color: Colors.blueGrey,
                      child: Column(
                        children: [
                          Expanded(child: Container()),
                          Text(
                            'Click to view',
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
                          ),
                          Expanded(child: Container()),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ))
        .toList();
  }

  void _openRadicalCardDetail(PortfolioInfo info) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => PageProjectDetails(info: info)));
  }
}
