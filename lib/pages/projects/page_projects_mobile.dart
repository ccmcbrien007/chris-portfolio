import 'package:chris_portfolio/model/portfolio_info.dart';
import 'package:chris_portfolio/pages/projects/page_projects.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../info.dart';
import '../../utils/fade_in_card.dart';
import '../../utils/portfolio_navigator.dart';

abstract class PageProjectsMobile extends StatefulWidget {
  const PageProjectsMobile({super.key});
}

abstract class _PageProjectsMobileState extends State<PageProjectsMobile> {
  bool animationDone = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text(Info.appTitle),
      ),
      drawer: PortfolioNavigator.getMobileDrawer(context, PageProjects.name),
      body: Center(
        child: Row(
          children: <Widget>[
            TweenAnimationBuilder<int>(
              tween: IntTween(begin: 100, end: 0),
              curve: Curves.easeOut,
              duration: const Duration(seconds: 1),
              builder: (BuildContext context, int value, Widget? child) {
                return Expanded(
                  flex: value,
                  child: Container(color: Colors.black),
                );
              },
              onEnd: () => setState(() {
                animationDone = true;
              }),
            ),
            Expanded(
              flex: 100,
              child: Container(
                padding: const EdgeInsets.fromLTRB(15, 16, 5, 5),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: !animationDone ? Container() : _getPageContent(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getPageContent(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: Info.portfolioItems.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildPortfolioCard(context, index);
        });
  }

  Widget _buildPortfolioCard(BuildContext context, int index) {
    PortfolioInfo info = Info.portfolioItems.elementAt(index);
    Size screenSize = MediaQuery.of(context).size;

    return InkWell(
      onTap: () {
        _openRadicalCardDetail(info);
      },
      child: Container(
        width: screenSize.width * .9,
        height: 120,
        color: const Color.fromRGBO(238, 230, 230, 1),
        child: Stack(
          children: [
            ListTile(
              leading: Image.network(
                "assets/images/${info.imagePath}",
                fit: BoxFit.contain,
              ),
              title: Text('${info.name}\n${info.tech}', style: Theme.of(context).textTheme.labelLarge),
              subtitle:
                  Text(info.description, textAlign: TextAlign.center, style: Theme.of(context).textTheme.labelSmall),
            ),
            FadeInCard(
              width: screenSize.width,
              height: 120,
              duration: 500,
              child: Container(
                color: Colors.blueGrey,
                child: Column(
                  children: [
                    Expanded(child: Container()),
                    Text(
                      'Click to view',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
                    ),
                    Expanded(child: Container()),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _openRadicalCardDetail(PortfolioInfo info) {
    launchUrl(Uri.parse('https://gitlab.com/ccmcbrien007/radicalcard'));
  }
}

class PageProjectsMobilePort extends PageProjectsMobile {
  const PageProjectsMobilePort({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageProjectsMobilePortState();
  }
}

class _PageProjectsMobilePortState extends _PageProjectsMobileState {}

class PageProjectsMobileLand extends PageProjectsMobile {
  const PageProjectsMobileLand({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageProjectsMobileLandState();
  }
}

class _PageProjectsMobileLandState extends _PageProjectsMobileState {}
