import 'package:flutter/material.dart';
import 'package:icons_plus/icons_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../info.dart';
import '../../model/portfolio_info.dart';
import '../../utils/portfolio_navigator.dart';

class PageProjectDetails extends StatefulWidget {
  static const name = Info.pageNamePortfolio;

  final PortfolioInfo info;

  const PageProjectDetails({super.key, required this.info});

  @override
  State<StatefulWidget> createState() {
    return _PageProjectDetailsState();
  }
}

class _PageProjectDetailsState extends  State<PageProjectDetails> {
  bool animationDone = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: PortfolioNavigator.getAppBar(context, PageProjectDetails.name),
      body: Center(
        child: Row(
          children: <Widget>[
            TweenAnimationBuilder<int>(
              tween: IntTween(begin: 100, end: 10),
              curve: Curves.easeOut,
              duration: const Duration(seconds: 1),
              builder: (BuildContext context, int value, Widget? child) {
                return Expanded(
                  flex: value,
                  child: Container(color: Colors.black),
                );
              },
              onEnd: () => setState(() {
                animationDone = true;
              }),
            ),
            Expanded(
              flex: 100,
              child: Container(
                padding: const EdgeInsets.fromLTRB(15, 80, 5, 5),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: !animationDone ? Container() : _getPageContent(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getPageContent(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.topLeft,
            child: Image.asset(
              "images/${widget.info.imagePath}",
              fit: BoxFit.contain,
            ),
          ),
        ),
        Expanded(child: Container(
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(widget.info.name, style: Theme.of(context).textTheme.headlineMedium),
              Container(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Text(widget.info.description, textAlign: TextAlign.center, style: Theme.of(context).textTheme.labelLarge)),
              Text("Built with ${widget.info.tech}", style: Theme.of(context).textTheme.titleMedium),
              IconButton(
                icon: Logo(Logos.gitlab),
                onPressed: () => {launchUrl( Uri.parse(Info.uriGitlab) )},
              ),
            ],
          ),
        ),)
      ],
    );  
  }
}