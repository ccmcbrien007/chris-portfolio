import 'package:chris_portfolio/model/certificate.dart';
import 'package:chris_portfolio/pages/certifications/page_certificate.dart';
import 'package:chris_portfolio/pages/certifications/page_certifications.dart';
import 'package:chris_portfolio/utils/fade_in_card.dart';
import 'package:flutter/material.dart';

import '../../info.dart';
import '../../utils/portfolio_navigator.dart';

class PageCertificationsDesktop extends StatefulWidget {
  static const name = Info.pageNameCerts;

  const PageCertificationsDesktop({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageCertificationsState();
  }
}

class _PageCertificationsState extends State<PageCertificationsDesktop> with TickerProviderStateMixin {
  bool animationDone = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: PortfolioNavigator.getAppBar(context, PageCertifications.name),
      body: Center(
        child: Row(
          children: <Widget>[
            TweenAnimationBuilder<int>(
              tween: IntTween(begin: 100, end: 10),
              curve: Curves.easeOut,
              duration: const Duration(seconds: 1),
              builder: (BuildContext context, int value, Widget? child) {
                return Expanded(
                  flex: value,
                  child: Container(color: Colors.black),
                );
              },
              onEnd: () => setState(() {
                animationDone = true;
              }),
            ),
            Expanded(
              flex: 100,
              child: Container(
                padding: const EdgeInsets.fromLTRB(5, 16, 5, 5),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: !animationDone ? Container() : _getPageContent(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getPageContent(BuildContext context) {
    List<Widget> cards = _buildExperienceCards();

    return GridView.count(primary: true, crossAxisCount: 2, children: [
      ...cards,
    ]);
  }

  List<Widget> _buildExperienceCards() {
    return Info.certs.map((cert) => _buildExperienceCard(cert)).toList();
  }

  Widget _buildExperienceCard(Certificate cert) {
    Size screenSize = MediaQuery.of(context).size;
    var orientation = MediaQuery.of(context).orientation;

    double widgetWidth =
        (orientation == Orientation.portrait) ? screenSize.shortestSide * 0.9 : screenSize.longestSide * 0.9;
    double widgetHeight = screenSize.longestSide * 0.3;


    return InkWell(
      onTap: () {
        _openExperienceInfo(cert);
      },
      child: Container(
        width: widgetWidth,
        height: widgetHeight,
        padding: const EdgeInsets.all(15),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image.network(
              cert.thumbnailPath,
              fit: BoxFit.contain,
            ),
            FadeInCard(
              width: widgetWidth,
              height: widgetHeight,
              duration: 500,
              child: Container(
                color: Colors.blueGrey,
                child: Column(
                  children: <Widget>[
                    Expanded(child: Container()),
                    Text(
                      cert.name,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
                    ),
                    Text(
                      cert.awardedBy,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
                    ),
                    Text(
                      '> Click to view <',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
                    ),
                    Expanded(child: Container()),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _openExperienceInfo(Certificate cert) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => PageCertificate(cert: cert)));
  }
}
