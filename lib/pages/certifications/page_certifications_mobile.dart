import 'package:chris_portfolio/model/certificate.dart';
import 'package:chris_portfolio/pages/certifications/page_certifications.dart';
import 'package:chris_portfolio/utils/fade_in_card.dart';
import 'package:flutter/material.dart';

import '../../info.dart';
import '../../utils/portfolio_navigator.dart';

abstract class PageCertificationsMobile extends StatefulWidget {
  static const name = Info.pageNameCerts;

  const PageCertificationsMobile({super.key});
}

abstract class _PageCertificationsMobileState extends State<PageCertificationsMobile> with TickerProviderStateMixin {
  bool animationDone = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text(Info.appTitle),
      ),
      drawer: PortfolioNavigator.getMobileDrawer(context, PageCertifications.name),
      body: Center(
        child: Row(
          children: <Widget>[
            TweenAnimationBuilder<int>(
              tween: IntTween(begin: 100, end: 0),
              curve: Curves.easeOut,
              duration: const Duration(seconds: 1),
              builder: (BuildContext context, int value, Widget? child) {
                return Expanded(
                  flex: value,
                  child: Container(color: Colors.black),
                );
              },
              onEnd: () => setState(() {
                animationDone = true;
              }),
            ),
            Expanded(
              flex: 100,
              child: Container(
                padding: const EdgeInsets.fromLTRB(5, 16, 5, 5),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: !animationDone ? Container() : _getPageContent(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getPageContent(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: Info.certs.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildCertCard(context, index);
        });
  }

  Widget _buildCertCard(BuildContext context, int index) {
  Certificate cert = Info.certs.elementAt(index);
  Size screenSize = MediaQuery.of(context).size;
  var orientation = MediaQuery.of(context).orientation;

  double widgetWidth = (orientation == Orientation.portrait) ?
    screenSize.shortestSide * 0.9 : screenSize.longestSide * 0.9;
  double widgetHeight = (orientation == Orientation.portrait) ?
    screenSize.longestSide / 4 : screenSize.shortestSide * 0.9;


    return InkWell(
      onTap: () {
        _openExperienceInfo(cert);
      },
      child: Container(
        width: widgetWidth,
        height: widgetHeight,
        padding: const EdgeInsets.all(15),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image.network(
              width: widgetWidth,
              height: widgetHeight,
              cert.thumbnailPath,
              fit: BoxFit.contain,
            ),
            FadeInCard(
              width: widgetWidth,
              height: widgetHeight,
              duration: 500,
              child: Container(
                color: Colors.blueGrey,
                child: Column(
                  children: <Widget>[
                    Expanded(child: Container()),
                    Text(
                      cert.name,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
                    ),
                    Text(
                      cert.awardedBy,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
                    ),
                    Expanded(child: Container()),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _openExperienceInfo(Certificate cert) {
    //Navigator.push(context, MaterialPageRoute(builder: (context) => PageCertificate(cert: cert)));
  }
}

class PageCertificationsMobilePort extends PageCertificationsMobile {
  const PageCertificationsMobilePort({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageCertificationsMobilePortState();
  }
}

class _PageCertificationsMobilePortState extends _PageCertificationsMobileState {}

class PageCertificationsMobileLand extends PageCertificationsMobile {
  const PageCertificationsMobileLand({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageCertificationsMobileLandState();
  }
}

class _PageCertificationsMobileLandState extends _PageCertificationsMobileState {}
