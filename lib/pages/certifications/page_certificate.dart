import 'package:chris_portfolio/pages/certifications/page_certifications.dart';
import 'package:flutter/material.dart';

import '../../model/certificate.dart';
import '../../utils/portfolio_navigator.dart';

class PageCertificate extends StatefulWidget {
  final Certificate cert;

  const PageCertificate({super.key, required this.cert});

  @override
  State<StatefulWidget> createState() {
    return _PageCertificateState();
  }
}

class _PageCertificateState extends  State<PageCertificate> {
  bool animationDone = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: PortfolioNavigator.getAppBar(context, PageCertifications.name),
      body: Center(
        child: Row(
          children: <Widget>[
            TweenAnimationBuilder<int>(
              tween: IntTween(begin: 100, end: 10),
              curve: Curves.easeOut,
              duration: const Duration(seconds: 1),
              builder: (BuildContext context, int value, Widget? child) {
                return Expanded(
                  flex: value,
                  child: Container(color: Colors.black),
                );
              },
              onEnd: () => setState(() {
                animationDone = true;
              }),
            ),
            Expanded(
              flex: 100,
              child: Container(
                padding: const EdgeInsets.fromLTRB(15, 80, 5, 5),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: !animationDone ? Container() : _getPageContent(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getPageContent(BuildContext context) {
    return Center(
      child: Image.network(widget.cert.thumbnailPath),
    );
  }
}