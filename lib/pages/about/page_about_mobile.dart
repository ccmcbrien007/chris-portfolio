import 'package:chris_portfolio/config.dart';
import 'package:chris_portfolio/pages/about/page_about.dart';
import 'package:chris_portfolio/utils/proficiency_part.dart';
import 'package:flutter/material.dart';
import 'package:typewritertext/typewritertext.dart';

import '../../info.dart';
import '../../utils/portfolio_navigator.dart';

abstract class PageAboutMobile extends StatefulWidget {
  const PageAboutMobile({super.key});
}

abstract class _PageAboutMobileState extends State<PageAboutMobile> {
  bool animationDone = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text(Info.appTitle),
      ),
      drawer: PortfolioNavigator.getMobileDrawer(context, PageAbout.name),
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TweenAnimationBuilder<int>(
            tween: IntTween(begin: 100, end: 0),
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: Config.pageOpenAnimationDuration),
            builder: (BuildContext context, int value, Widget? child) {
              return Expanded(
                flex: value,
                child: Container(color: Colors.black),
              );
            },
            onEnd: () => setState(() {
              animationDone = true;
            }),
          ),
          Expanded(
            flex: 100,
            child: Container(
              padding: const EdgeInsets.fromLTRB(15, 8, 5, 5),
              alignment: Alignment.topLeft,
              color: Colors.white,
              child: !animationDone ? Container() : _getPageContent(context),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getPageContent(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    var orientation = MediaQuery.of(context).orientation;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        TypeWriterText(
            repeat: false,
            text: Text(Info.introText,
                textAlign: TextAlign.left,
                style: (orientation == Orientation.portrait && screenSize.shortestSide < 1000)
                    ? Theme.of(context).textTheme.bodyMedium
                    : Theme.of(context).textTheme.labelLarge),
            duration: const Duration(milliseconds: 25)),
        TypeWriterText(
            repeat: false,
            text: Text(Info.introName,
                textAlign: TextAlign.left,
                style: (orientation == Orientation.portrait && screenSize.shortestSide < 1000)
                    ? Theme.of(context).textTheme.bodyLarge
                    : Theme.of(context).textTheme.headlineLarge),
            duration: const Duration(milliseconds: 25)),
        TypeWriterText(
            repeat: false,
            text: Text(Info.introTitle,
                textAlign: TextAlign.left,
                style: (orientation == Orientation.portrait && screenSize.shortestSide < 1000)
                    ? Theme.of(context).textTheme.bodyLarge
                    : Theme.of(context).textTheme.headlineLarge),
            duration: const Duration(milliseconds: 25)),
        const Divider(),
        Container(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: TypeWriterText(
              repeat: false,
              text: Text(Info.introDescText,
                  textAlign: TextAlign.left,
                  style: (orientation == Orientation.portrait && screenSize.shortestSide < 1000)
                      ? Theme.of(context).textTheme.bodySmall
                      : Theme.of(context).textTheme.labelLarge),
              duration: const Duration(milliseconds: 25)),
        ),
        Expanded(child: Center(child: _buildProficiencyList())),
      ],
    );
  }

  Widget _buildProficiencyList() {
    List<ProficiencyPart> proficiencies = Info.proficiencies
        .map((proficiency) => ProficiencyPart(
              name: proficiency.name,
              mastery: proficiency.mastery,
            ))
        .toList();
    proficiencies.sort((b, a) => a.mastery.compareTo(b.mastery));

    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Wrap(
            runSpacing: 16,
            children: [
              ...proficiencies,
            ],
          ),
        ],
      ),
    );
  }
}

class PageAboutMobilePort extends PageAboutMobile {
  const PageAboutMobilePort({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageAboutMobilePortState();
  }
}

class _PageAboutMobilePortState extends _PageAboutMobileState {}

class PageAboutMobileLand extends PageAboutMobile {

  const PageAboutMobileLand({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageAboutMobileLandState();
  }
}

class _PageAboutMobileLandState extends _PageAboutMobileState {
}
