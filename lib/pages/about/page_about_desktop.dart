import 'package:chris_portfolio/config.dart';
import 'package:chris_portfolio/pages/about/page_about.dart';
import 'package:chris_portfolio/utils/proficiency_part.dart';
import 'package:flutter/material.dart';

import 'package:typewritertext/typewritertext.dart';

import '../../info.dart';
import '../../utils/portfolio_navigator.dart';

class PageAboutDesktop extends StatefulWidget {
  static const name = Info.pageNameAbout;

  const PageAboutDesktop({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PageAboutState();
  }
}

class _PageAboutState extends State<PageAboutDesktop> {
  bool animationDone = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.black,
      appBar: PortfolioNavigator.getAppBar(context, PageAbout.name),
      body: Center(
        child: Row(
          children: <Widget>[
            TweenAnimationBuilder<int>(
              tween: IntTween(begin: 100, end: 10),
              curve: Curves.easeOut,
              duration: const Duration(milliseconds: Config.pageOpenAnimationDuration),
              builder: (BuildContext context, int value, Widget? child) {
                return Expanded(
                  flex: value,
                  child: Container(color: Colors.black),
                );
              },
              onEnd: () => setState(() {
                animationDone = true;
              }),
            ),
            Expanded(
              flex: 100,
              child: Container(
                padding: const EdgeInsets.fromLTRB(15, 16, 5, 5),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: !animationDone ? Container() : _getPageContent(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getPageContent(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        TypeWriterText(
            repeat: false,
            text: Text(Info.introText, textAlign: TextAlign.left, style: Theme.of(context).textTheme.labelLarge),
            duration: const Duration(milliseconds: 25)),
        TypeWriterText(
            repeat: false,
            text: Text(Info.introName, textAlign: TextAlign.left, style: Theme.of(context).textTheme.headlineLarge),
            duration: const Duration(milliseconds: 25)),
        TypeWriterText(
            repeat: false,
            text: Text(Info.introTitle, textAlign: TextAlign.left, style: Theme.of(context).textTheme.headlineLarge),
            duration: const Duration(milliseconds: 25)),
        const Divider(),
        Container(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: TypeWriterText(
              repeat: false,
              text: Text(Info.introDescText, textAlign: TextAlign.left, style: Theme.of(context).textTheme.labelLarge),
              duration: const Duration(milliseconds: 25)),
        ),
        Expanded(child: _buildProficiencyList()),
      ],
    );
  }

  Widget _buildProficiencyList() {
    List<ProficiencyPart> proficiencies = Info.proficiencies
        .map((proficiency) => ProficiencyPart(
              name: proficiency.name,
              mastery: proficiency.mastery,
            ))
        .toList();
    proficiencies.sort((b, a) => a.mastery.compareTo(b.mastery));

    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Wrap(
          runSpacing: 16,
          children: [
            ...proficiencies,
          ],
        ),
      ],
    );
  }
}
