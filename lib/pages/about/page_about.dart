import 'package:chris_portfolio/pages/about/page_about_desktop.dart';
import 'package:chris_portfolio/pages/about/page_about_mobile.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../info.dart';

class PageAbout extends StatelessWidget {
  static const name = Info.pageNameAbout;

  const PageAbout({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      breakpoints: const ScreenBreakpoints(desktop: 1000, tablet: 900, watch: 300),
      mobile: (context) => OrientationLayoutBuilder(
          portrait: (context) => const PageAboutMobilePort(), landscape: (context) => const PageAboutMobileLand()),
      desktop: (context) => const PageAboutDesktop(),
      //watch: (BuildContext context) => _getMobileLayout(theme),
      tablet: (context) => const PageAboutDesktop(),
    );
  }
}
