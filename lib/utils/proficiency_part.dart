import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ProficiencyPart extends StatefulWidget {
  final String name;
  final double mastery;

  const ProficiencyPart({super.key, this.name = "", this.mastery = 0});

  @override
  State<ProficiencyPart> createState() => _ProficiencyPartState();
}

class _ProficiencyPartState extends State<ProficiencyPart> with TickerProviderStateMixin {
  late AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      /// [AnimationController]s can be created with `vsync: this` because of
      /// [TickerProviderStateMixin].
      vsync: this,
      duration: const Duration(seconds: 2),
      upperBound: widget.mastery,
    )..addListener(() {
        setState(() {});
      });
    controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double widthOfScreen = MediaQuery.of(context).size.width;
    var orientation = MediaQuery.of(context).orientation;
    var desktopSize = ResponsiveSizingConfig.instance.breakpoints.desktop;
    double widgetWidth = (orientation == Orientation.portrait && widthOfScreen < desktopSize)
        ? (widthOfScreen / 1.25)
        : (widthOfScreen * 0.4);

    return SizedBox(
      width: widgetWidth,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircularProgressIndicator(
                  value: controller.value / 10,
                  semanticsLabel: 'Proficiency level',
                  semanticsValue: widget.mastery.toString(),
                ),
              ),
              Text(widget.mastery.toString()),
            ],
          ),
          Flexible(child: Align(alignment: Alignment.centerLeft, child: Text(textAlign: TextAlign.left, widget.name))),
        ],
      ),
    );
  }
}
