import 'package:flutter/material.dart';
import 'package:icons_plus/icons_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../info.dart';

class WebPresence extends StatelessWidget {
  const WebPresence({super.key});

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var screenSize = MediaQuery.of(context).size;

    return (orientation == Orientation.portrait || screenSize.width > 900)
        ? Row(children: _getIconButtons())
        : Column(
            children: _getIconButtons(),
          );
  }

  List<Widget> _getIconButtons() {
    return [
      IconButton(
        icon: Logo(size: 24, Logos.gitlab),
        onPressed: () => {launchUrl(Uri.parse(Info.uriGitlab))},
      ),
      IconButton(
        icon: Logo(size: 24, Logos.linkedin),
        onPressed: () => {launchUrl(Uri.parse(Info.urlLinkedIn))},
      ),
      IconButton(
        icon: Logo(size: 24, Logos.pluralsight),
        onPressed: () => {launchUrl(Uri.parse(Info.urlPluralsight))},
      ),
      IconButton(
        icon: Logo(size: 24, Logos.salesforce),
        onPressed: () => {launchUrl(Uri.parse(Info.urlSalesforce))},
      )
    ];
  }
}
