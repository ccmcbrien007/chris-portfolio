import 'package:chris_portfolio/pages/home/page_home.dart';
import 'package:chris_portfolio/pages/about/page_about.dart';
import 'package:chris_portfolio/pages/certifications/page_certifications.dart';
import 'package:chris_portfolio/pages/experience/page_experience.dart';
import 'package:chris_portfolio/pages/projects/page_projects.dart';
import 'package:chris_portfolio/utils/web_presence.dart';
import 'package:flutter/material.dart';

class PortfolioNavigator {
  static final List<Destination> _destinations = [
    Destination(
        label: PageHome.name, buildRoute: (context) => MaterialPageRoute(builder: (context) => const PageHome())),
    Destination(
        label: PageAbout.name, buildRoute: (context) => MaterialPageRoute(builder: (context) => const PageAbout())),
    Destination(
        label: PageExperience.name, buildRoute: (context) => MaterialPageRoute(builder: (context) => const PageExperience())),
    Destination(
        label: PageProjects.name, buildRoute: (context) => MaterialPageRoute(builder: (context) => const PageProjects())),
    Destination(
        label: PageCertifications.name,
        buildRoute: (context) => MaterialPageRoute(builder: (context) => const PageCertifications())),
  ];

  static PreferredSizeWidget getAppBar(BuildContext context, String selectedPage) {
    Size screenSize = MediaQuery.of(context).size;

    return PreferredSize(
      preferredSize: Size(screenSize.width, 1000),
      child: Container(
        color: Colors.blueGrey.withOpacity(0.5),
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            children: [
              Text(
                'EXPLORE',
                style: TextStyle(
                  color: Colors.blueGrey[100],
                  fontSize: 20,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w400,
                  letterSpacing: 3,
                ),
              ),
              SizedBox(width: screenSize.width / 20),
              NavigationLinks(selectedPage: selectedPage, screenSize: screenSize),
              SizedBox(width: screenSize.width / 20),
              const WebPresence(),
            ],
          ),
        ),
      ),
    );
  }

  static double getMobileDrawerSize(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    return orientation == Orientation.portrait ? 250 : 175;
  }

  static Widget getMobileDrawer(BuildContext context, String selectedPageName) {

    return Drawer(
      width: getMobileDrawerSize(context),
      shadowColor: Colors.white,
      backgroundColor: Colors.grey,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            child: Text(
              'EXPLORE',
              style: TextStyle(
                color: Colors.blueGrey[100],
                fontSize: 20,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w400,
                letterSpacing: 3,
              ),
            ),
          ),
          ..._destinations
              .map((dest) => ListTile(
                    title: Text(dest.label),
                    selected: dest.label == selectedPageName,
                    onTap: () {
                      Navigator.push(context, dest.buildRoute(context));
                    },
                  ))
              .toList(),
          const WebPresence(),
        ],
      ),
    );
  }
}

class NavigationLinks extends StatefulWidget {
  final String selectedPage;
  final Size screenSize;

  const NavigationLinks({super.key, required this.selectedPage, required this.screenSize});

  @override
  State<StatefulWidget> createState() {
    return _NavigationLinksState();
  }
}

class _NavigationLinksState extends State<NavigationLinks> {
  String _hoveredMenuItem = "";

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        ...PortfolioNavigator._destinations
            .map((dest) => getPageItem(dest.label, context: context, route: dest.buildRoute(context)))
            .toList(),
      ]),
    );
  }

  Widget getPageItem(String pageName, {required BuildContext context, required MaterialPageRoute route}) {
    return InkWell(
      onHover: (value) {
        setState(() {
          _hoveredMenuItem = (value) ? pageName : "";
        });
      },
      onTap: () {
        Navigator.push(context, route);
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(widget.screenSize.width / 100, 0, widget.screenSize.width / 100, 0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              pageName,
              textAlign: TextAlign.left,
              style: TextStyle(color: (_hoveredMenuItem == pageName) ? Colors.black87 : Colors.white),
            ),
            const SizedBox(height: 5),
            Visibility(
              maintainAnimation: true,
              maintainState: true,
              maintainSize: true,
              visible: (_hoveredMenuItem == pageName),
              child: Container(height: 2, width: 20, color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}

class Destination {
  final String label;
  final MaterialPageRoute Function(BuildContext context) buildRoute;

  const Destination({required this.label, required this.buildRoute});
}
