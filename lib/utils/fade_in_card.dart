import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FadeInCard extends StatefulWidget {
  final Widget child;
  final int duration;
  final EdgeInsets? padding;
  final double height;
  final double width;

  const FadeInCard(
      {super.key, required this.child, this.duration = 500, this.padding, this.height = 0, this.width = 0});

  @override
  State<StatefulWidget> createState() {
    return _FadeInCardState();
  }
}

class _FadeInCardState extends State<FadeInCard> {
  bool _visible = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.padding ?? EdgeInsets.zero,
      height: widget.height,
      width: widget.width,
      child: MouseRegion(
        onEnter: (e) => _mouseEnter(true),
        onExit: (e) => _mouseEnter(false),
        child: AnimatedOpacity(
          opacity: _visible ? 0.85 : 0.0,
          duration: Duration(milliseconds: widget.duration),
          child: widget.child,
        ),
      ),
    );
  }

  _mouseEnter(bool visible) {
    setState(() {
      _visible = visible;
    });
  }
}
