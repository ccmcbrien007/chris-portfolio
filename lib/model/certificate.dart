class Certificate {
  final String name;
  final String url;
  final String thumbnailPath;
  final double imageSize;
  final String awardedBy;

  const Certificate(
      {required this.name,
      required this.url,
      required this.thumbnailPath,
      required this.imageSize,
      required this.awardedBy});
}
