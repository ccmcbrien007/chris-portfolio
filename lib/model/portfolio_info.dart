class PortfolioInfo {
  final String name;
  final String tech;
  final String description;
  final String imagePath;
  final String gitlabUri;

  const PortfolioInfo(
      {required this.name,
      required this.tech,
      required this.description,
      required this.imagePath,
      required this.gitlabUri});
}
