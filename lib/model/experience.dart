class Experience {
  final String dateRange;
  final List<String> responsibilities;
  final String role;
  final String company;

  const Experience({required this.company, required this.role, required this.responsibilities, required this.dateRange});
}