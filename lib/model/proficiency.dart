class Proficiency {
  final String name;
  final double mastery;

  const Proficiency({this.name  = "", this.mastery  = 0});
}