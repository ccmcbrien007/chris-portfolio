import 'package:chris_portfolio/model/experience.dart';
import 'package:chris_portfolio/model/proficiency.dart';

import 'model/certificate.dart';
import 'model/portfolio_info.dart';

class Info {
  static const String appChrisMcBrien = "Chris McBrien";
  static const String appTitle = "Professional Portfolio";
  static const String appSubTitle = "SOLUTION ARCHITECT";
  static const String designedIn = "Made in Canada with \u{2764} using Flutter & Dart";
  //static const String designedUsing = " using Flutter & Dart";
  static const String quote = "There is nothing in a caterpillar that tells you it's going to be a butterfly.";
  static const String quoteAttribution = "- Buckminster Fuller";

  static const String pageNameHome = "Home";
  static const String pageNameAbout = "About Me";
  static const String pageNamePortfolio = "Projects";
  static const String pageNameExperience = "Experience";
  static const String pageNameCerts = "Certifications";

  static const String uriGitlab = "https://gitlab.com/ccmcbrien007";
  static const String urlLinkedIn = "https://www.linkedin.com/in/ccmcbrien";
  static const String urlPluralsight = "https://app.pluralsight.com/profile/chris-mcbrien";
  static const String urlSalesforce = "https://www.salesforce.com/trailblazer/chris-mcbrien";

  static const String introText = "Hello and welcome";
  static const String introName = "My name is Chris McBrien";
  static const String introTitle = "Builder of things";
  static const String introDescText =
      "I am a full-stack web and Salesforce developer, scrum master, and solution architect.";

  static const List<Proficiency> proficiencies = [
    Proficiency(name: "Salesforce admin", mastery: 9.5),
    Proficiency(name: "Java/EE/Core/JPA/JMS", mastery: 8.0),
    Proficiency(name: "Postgres/SQLServer/Oracle/Firestore", mastery: 9.5),
    Proficiency(name: "REST, JSON, XML, Postman,", mastery: 7.5),
    Proficiency(name: "Flutter/Dart/Adroid Studio/Web", mastery: 5.5),
    Proficiency(name: "C#/.Net", mastery: 7.5),
    Proficiency(name: "JavaScript/TypeScript/Angular", mastery: 5.0),
    Proficiency(name: "Linux Admin", mastery: 6.5),
    Proficiency(name: "Git/Hg/Subversion/Azue DevOps/Jenkins", mastery: 7.0),
    Proficiency(name: "iMIS Admin", mastery: 8.0),
    Proficiency(name: "Agile/Scrum", mastery: 6.5),
    Proficiency(name: "HTML/CSS/ReST", mastery: 9.0),
    Proficiency(name: "Google Cloud Platform", mastery: 4.5),
    Proficiency(name: "Design Patterns/Solution Architecture", mastery: 9.0),
    Proficiency(name: "Jira/Confluence/Crucible", mastery: 3.5),
    Proficiency(name: "Testing/JUnit/SonarQube", mastery: 8.0),
  ];

  static const List<PortfolioInfo> portfolioItems = [
    PortfolioInfo(
        imagePath: "multi-feature-1.png",
        name: "Radical Card",
        tech: "Dart/Flutter",
        gitlabUri: "https://gitlab.com/ccmcbrien007/radicalcard",
        description: "A mobile app to manage and exchange business cards and contact information"),
  ];

  static const List<Experience> experiences = [
    Experience(
        company: "Cloud Pilots",
        role: "Solution Architect",
        responsibilities: [
          "Grew product offerings and revenue",
          "Business analysis and customer engagement for ~dozen clients",
          "User testing, debugging, support and maintenance",
          "Evaluations, escalations and technical requirements documents"
        ],
        dateRange: "August 2019 - August 2023"),
    Experience(
        company: "Energy Safety Canada",
        role: "Solution Architect",
        responsibilities: [
          "Developed and enhanced enterprise systems",
          "Built systems and support for >100 direct and distance courses",
          "Evolved ERP developed new distance learning and mobile app"
        ],
        dateRange: "February 2018 - August 2019"),
    Experience(
        company: "Radical Speed Inc",
        role: "CTO",
        responsibilities: [
          "Started my small business degigning and marketing gear for exteme sports",
          "Built the IT infrastructure for online presence/web store, order management, and accounting integration",
          "Java/EJB, GWT, Google Cloud Platform, BigTable, Authorize.net"
        ],
        dateRange: "November 2015 - January 2018"),
    Experience(
        company: "Shaw Communications",
        role: "Senior Developer/Tech Team Lead",
        responsibilities: [
          "Successfully delivered solutions with net yearly saving over \$1 million",
          "Hired, trained and mentored staff to maximize effectiveness",
          "Directed and supervised team of 5 engaged in enterprise systems design and development",
          "Conducted training and mentored team members to promote productivity and commitment to outstanding service",
          "Built strong relationships with customers through positive attitude and attentive response",
          "Assisted in recruitment of new team members, hiring highest qualified to build team of top performers",
        ],
        dateRange: "November 2010 - November 2015"),
  ];

  static const List<Certificate> certs = [
    Certificate(
      name: "Salesforce Administrator",
      url: "Cert3731629_Administrator.png",
      thumbnailPath: "assets/images/certificates/Cert3731629_Administrator.png",
      imageSize: 0.30,
      awardedBy: "Salesforce",
    ),
    Certificate(
      name: "Software Architecture: Domain-Driven Design",
      url: "ddd.png",
      thumbnailPath: "assets/images/certificates/ddd.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Java Lambda Expressions",
      url: "java_lambda.png",
      thumbnailPath: "assets/images/certificates/java_lambda.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Java EE 8: JSON-B",
      url: "json_b.png",
      thumbnailPath: "assets/images/certificates/json_b.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Flutter Widgets",
      url: "flutter_03.png",
      thumbnailPath: "assets/images/certificates/flutter_03.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Flutter Application State",
      url: "flutter_04.png",
      thumbnailPath: "assets/images/certificates/flutter_04.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Flutter and Dart Packages",
      url: "flutter_05.png",
      thumbnailPath: "assets/images/certificates/flutter_05.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Modularizing and Packaging Flutter",
      url: "flutter_06.png",
      thumbnailPath: "assets/images/certificates/flutter_06.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Building Flutter UIs",
      url: "flutter_07.png",
      thumbnailPath: "assets/images/certificates/flutter_07.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Flutter Live Web Data",
      url: "flutter_08.png",
      thumbnailPath: "assets/images/certificates/flutter_08.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Dart & Cupertino Wdigets",
      url: "flutter_09.png",
      thumbnailPath: "assets/images/certificates/flutter_09.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
    Certificate(
      name: "Firebase Cloud Firestore",
      url: "flutter_10.png",
      thumbnailPath: "assets/images/certificates/flutter_10.png",
      imageSize: 0.30,
      awardedBy: "Lynda.com",
    ),
  ];
}
